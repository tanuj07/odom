#!/usr/env/bin python3

"""
Author(s):
Tanuj Thakkar (tanuj@phantomauto.com)
"""

# Importing packages
import can
import argparse
import sys
import os
from datetime import datetime
import pandas as pd
import time

# Constant parameters
STEERING_REPORT_MAX_LEFT = 648.0
STEERING_REPORT_MAX_RIGHT = 623.0
STEERING_REPORT_CENTER_OFFSET = 51
STEERING_LIMIT = 0.25
M_PI_2 = 3.14
FLOAT16_MAX = 65535

# CAN message parameters
TRACTION_TPDO_COBID = 904 # HEX = 0x388 : Arbitration ID for speed
EPS_TPDO_COBID = 920 # HEX = 0x398 : Arbitration ID for steering


def normalizeNumOnRange(num: float, inMin: float, inMax: float, outMin: float, outMax: float) -> float:
    return (num - inMin) * (outMax - outMin) / (inMax - inMin) + outMin

def main() -> None:

    parser = argparse.ArgumentParser()
    parser.add_argument('--channel_id', type=str, default="can1", help='CAN ID to access')
    parser.add_argument('--save_path', type=str, default="../data", help='Path to data saving directory')
    parser.add_argument('--test_name', type=str, default=None, help='Test name to save data file as')
    parser.add_argument('--disable_logs', action='store_true', help='Disable saving logs to CSV file')

    args = parser.parse_args()
    channel_id = args.channel_id
    save_path = args.save_path
    test_name = args.test_name
    disable_logs = args.disable_logs

    # Create a bus instance
    bus = can.Bus(interface='socketcan', channel=channel_id, receive_own_messages=True)
    
    # Data fields
    speed = None
    steering = None

    # Logging data in CSV file
    if(not disable_logs):
        os.makedirs(save_path, exist_ok=True)
        if(test_name == None):
            save_file = os.path.join(save_path, datetime.now().strftime("%Y%m%d-%H%M%S") + '.csv')
        else:
            save_file = os.path.join(save_path, test_name + '.csv')
        data = [time.time(), speed, steering]
        df = pd.DataFrame([data], columns=['Time (s)', 'Speed (km/h)', 'Steering (rad)'])
        df.to_csv(save_file, mode='a')

    # Iterate over received messages
    for msg in bus:
        
        # Reading TRACTION_TPDO_COBID messages
        if(msg.arbitration_id == TRACTION_TPDO_COBID):
            try:
                speed = ((msg.data[2] << 8) | msg.data[3])
                if(speed > 32000): speed = -(FLOAT16_MAX - speed)
                speed /= 1000

            except Exception as e:
                print(e)
                speed = None
                pass
        else:
            speed = None

        # Reading EPS_TPDO_COBID messages
        if(msg.arbitration_id == EPS_TPDO_COBID):
            try:
                steering = ((msg.data[1] << 8) | msg.data[2])
                steering += STEERING_REPORT_CENTER_OFFSET # Adding center offset
                if(steering > 32000):
                    steering = -(FLOAT16_MAX - steering)
                # steering = normalizeNumOnRange(steering, -STEERING_REPORT_MAX_LEFT, STEERING_REPORT_MAX_RIGHT, -M_PI_2*STEERING_LIMIT, M_PI_2*STEERING_LIMIT)
            except Exception as e:
                print(e)
                steering = None
                pass
        else:
            steering = None

        print("Speed: {} || Steering: {}".format(speed, steering))

        # Writing data to CSV
        if(not disable_logs):
            data = [time.time(), speed, steering]
            df = pd.DataFrame([data])
            df.to_csv(save_file, header=False, mode='a')

if __name__ == '__main__':
    main()
