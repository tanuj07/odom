#!/usr/env/bin python3

"""
Author(s):
Tanuj Thakkar (tanuj@phantomauto.com)
"""


# Importing packages
import argparse
import sys
import os
from datetime import datetime
import pandas as pd
import time
from math import sin, cos, tan, isnan
from matplotlib import pyplot as plt

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--data_file', type=str, default=None, help='Data file for reading speed and steering samples')

    args = parser.parse_args()
    data_file = args.data_file

    if(data_file):
        print("Reading data from {}\n".format(data_file))

        data = pd.read_csv(data_file)

        x = 0 # [m], x coordinate of the state
        y = 0 # [m], y coordinate of the state
        theta = 0 # [m], theta/yaw coordinate of the state
        time = 0 # [s], time

        # Lists to store state values
        xs = list()
        ys = list()
        thetas = list()

        L = 2.602 # [m], Wheelbase of SPXe80 Center Rider

        for msg in range(1,len(data)):
            dt = (data["Time (s)"][msg-1] - data["Time (s)"][msg])
            v = data["Speed (km/h)"][msg] * (5/18)
            alpha = data["Steering (rad)"][msg]

            if(isnan(dt) or isnan(v) or isnan(alpha)):
                continue
            print(v, alpha, dt)

            # Forklift model
            # vx = (v*cos(theta)*cos(alpha))
            # vy = (v*sin(theta)*cos(alpha))
            # v_th = -((v/L)*sin(alpha))

            # Bicycle model
            vx = v*cos(theta)
            vy = v*sin(theta)
            v_th = (v/L)*tan(alpha)

            delta_x = vx * dt
            delta_y = vy * dt
            delta_th = v_th * dt

            x += delta_x
            y += delta_y
            theta += delta_th

            xs.append(x)
            ys.append(y)
            thetas.append(theta)

            time = time + dt

            print("x: {} | y: {} | theta: {}".format(x, y, theta))
    else:
        print("No data file provided")

    plt.plot(ys, xs)
    plt.show()

if __name__ == '__main__':
    main()