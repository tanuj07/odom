#!/usr/env/bin python3

"""
Authors: 
Bhavik Mukesh Panchal (bhavik@phantomauto.com), 
Sharon Richu Shaji (sharon@phantomauto.com),
Tanuj Thakkar (tanuj@phantomauto.com)

Company: Phantom Auto
"""

# Importing packages
import can
import argparse
from math import sin, cos, pi
import rospy
import tf
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3


class Odom():
    def __init__(self) -> None:
        #Constant parameters
        self.STEERING_REPORT_MAX = 720.0
        self.M_PI_2 = 3.14
        self.FLOAT16_MAX = 65535
        self.TRACTION_TPDO_COBID = 904 # HEX = 0x388 : Arbitration ID for speed
        self.EPS_TPDO_COBID = 920 # HEX = 0x398 : Arbitration ID for steering

        parser = argparse.ArgumentParser()
        parser.add_argument('--channel_id', type=str, default="can1", help='CAN ID to access')
        args = parser.parse_args()
        self.channel_id = args.channel_id

        #Create Publisher and Broadcaster
        self.odom_pub = rospy.Publisher("odom", Odometry, queue_size=50)
        self.odom_broadcaster = tf.TransformBroadcaster()

        # initialize the driving velocity (v), wheel base (l), orientation angle (theta) and the steering angle (delta)
        self.x = 0.0 #in meters
        self.y = 0.0    #in meters
        self.th = 0.0   #in meters

    def normalizeNumOnRange(self, num: float, inMin: float, inMax: float, outMin: float, outMax: float) -> float:
        return (num - inMin) * (outMax - outMin) / (inMax - inMin) + outMin

    def get_speed_and_steering(self, msg):      
        '''
        This function receives and returns the steering angle and speed from CAN bus 

        Parameter
        @msg - CAN bus message

        Returns:
        @speed - speed of the forklift (km/hr)
        @steering - steering angle of the forklift (radians)
        '''
        # Reading TRACTION_TPDO_COBID messages
        if(msg.arbitration_id == self.TRACTION_TPDO_COBID):
            try:
                speed = ((msg.data[2] << 8) | msg.data[3])
                speed = speed/1000
                if(speed < 0): speed = -(self.FLOAT16_MAX - speed)

            except Exception as e:
                print("Speed Data Exception: {}".format(e))
                speed = None
                pass

        # Reading EPS_TPDO_COBID messages
        if(msg.arbitration_id == self.EPS_TPDO_COBID):
            try:
                steering = ((msg.data[1] << 8) | msg.data[2])
                steering = self.normalizeNumOnRange(-steering, -self.STEERING_REPORT_MAX, \
                                                    self.STEERING_REPORT_MAX, -self.M_PI_2, self.M_PI_2)
            except Exception as e:
                print("Steering Data Exception: {}".format(e))
                steering = None
                pass

        print("Speed: {} || Steering: {}".format(speed, steering))

        return speed, steering

    def get_odom_msg(self, speed, steering):
        '''
        This function converts the speed and steering angle received from 
        the CAN bus to forklift odometry message and publishes it.

        Parameters:
        @speed - speed of the forklift (km/hr)
        @steering - steering angle of the forklift (radians) 

        Returns:
        @odom_msg - ROS Odometry message
        '''
        v = speed * (5/18) #km/hr -> m/s
        l = 2  # in meters, wheelbase
        alpha = steering #0.1  # steering angle , in radians

        current_time = rospy.Time.now()

        # compute odometry in a typical way given the velocities of the robot
        dt = (current_time - self.last_time).to_sec()
        vx = (v*cos(self.th)*cos(alpha))
        vy = (v*sin(self.th)*cos(alpha))
        v_th = -((v/l)*sin(alpha))

        delta_x = vx * dt
        delta_y = vy * dt
        delta_th = v_th * dt

        self.x += delta_x
        self.y += delta_y
        self.th += delta_th

        # since all odometry is 6DOF we'll need a quaternion created from yaw
        odom_quat = tf.transformations.quaternion_from_euler(0, 0, self.th)

        # first, we'll publish the transform over tf
        self.odom_broadcaster.sendTransform(
            (self.x, self.y, 0.),
            odom_quat,
            current_time,
            "base_link",
            "odom"
        )
        # next, we'll publish the odometry message over ROS
        odom_msg = Odometry()
        odom_msg.header.stamp = self.data_reading_time
        odom_msg.header.frame_id = "odom"
        # set the position
        odom_msg.pose.pose = Pose(Point(self.x, self.y, 0.), Quaternion(*odom_quat))
        # set the velocity
        odom_msg.child_frame_id = "base_link"
        odom_msg.twist.twist = Twist(Vector3(vx, vy, 0), Vector3(0, 0, v_th))

        # publish the message
        self.odom_pub.publish(odom)

        self.last_time = current_time   

        return odom_msg   

    def get_odom(self):
        '''
        This function reads the wheel encoder data from the CAN bus,
        passes it through the forklift kinematic model to get forklift
        odometry message. 
        '''
        # Data fields
        speed = None
        steering = None
        
        # Create a bus instance
        bus = can.Bus(interface='socketcan', channel=self.channel_id, receive_own_messages=True)

        rate = rospy.Rate(100.0)      

        self.last_time = rospy.Time.now()
        # Iterate over received messages
        for msg in bus:
            self.data_reading_time = rospy.Time.now()
            speed, steering = self.get_speed_and_steering(msg)
            odom_msg = self.get_odom_msg(speed, steering)                
            rate.sleep()
      
if __name__ == '__main__':
    rospy.init_node('odometry_publisher')
    odom = Odom()
    odom.get_odom()